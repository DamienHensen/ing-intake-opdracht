package com.ing.zoo.models;

import java.util.Random;

import com.ing.zoo.interfaces.CanDoTricks;
import com.ing.zoo.interfaces.Omnivore;

public class Pig extends Animal implements Omnivore, CanDoTricks {
    private static String helloText = "splash";
    private String eatText;
    private String trick;

    public Pig(String name) {
        super(name, helloText);
    }

    @Override
    public void eatLeaves() {
        eatText = "munch munch oink";
        System.out.println(eatText);
    }

    @Override
    public void eatMeat() {
        eatText = "nomnomnom oink thx";
        System.out.println(eatText);
    }

    @Override
    public void performTrick() {
        Random random = new Random();
        int rnd = random.nextInt(2);
        if (rnd == 0) {
            trick = "rolls in the mud";
        } else {
            trick = "runs in circles";
        }
        System.out.println(trick);
    }
}
