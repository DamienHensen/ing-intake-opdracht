package com.ing.zoo.models;

public class Animal {
    private String name;
    private String helloText = "Hi!";

    public Animal(String name) {
        this.name = name;
    }

    public Animal(String name, String helloText) {
        this(name);

        this.helloText = helloText;
    }

    public void sayHello() {
        System.out.println(helloText);
    }

    @Override
    public String toString() {
        return name;
    }
}
