package com.ing.zoo.models;

import java.util.Random;

import com.ing.zoo.interfaces.CanDoTricks;
import com.ing.zoo.interfaces.Omnivore;

public class Monkey extends Animal implements Omnivore, CanDoTricks {
    private static String helloText = "oe oe aaaa";
    private String eatText;
    private String trick;

    public Monkey(String name) {
        super(name, helloText);
    }

    @Override
    public void eatLeaves() {
        eatText = "crunch-rustle";
        System.out.println(eatText);
    }

    @Override
    public void eatMeat() {
        eatText = "chew-rip";
        System.out.println(eatText);
    }

    @Override
    public void performTrick() {
        Random random = new Random();
        int rnd = random.nextInt(2);
        if (rnd == 0) {
            trick = "does a backflip";
        } else {
            trick = "climbs a tree";
        }
        System.out.println(trick);
    }

}
