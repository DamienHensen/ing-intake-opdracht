package com.ing.zoo.models;

import com.ing.zoo.interfaces.Carnivore;

public class Lion extends Animal implements Carnivore {
    private static String helloText = "roooaoaaaaar";
    private String eatText = "nomnomnom thx mate";

    public Lion(String name) {
        super(name, helloText);
    }

    @Override
    public void eatMeat() {
        System.out.println(eatText);
    }
}
