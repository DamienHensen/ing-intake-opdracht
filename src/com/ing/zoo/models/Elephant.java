package com.ing.zoo.models;

import com.ing.zoo.interfaces.Herbivore;

public class Elephant extends Animal implements Herbivore {
    private static String helloText = "tooooooooeet";
    private String eatText;

    public Elephant(String name) {
        super(name, helloText);
    }

    @Override
    public void eatLeaves() {
        eatText = "crunch-munch";
        System.out.println(eatText);
    }
}
