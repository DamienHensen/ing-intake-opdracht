package com.ing.zoo.models;

import com.ing.zoo.interfaces.Herbivore;

public class Hippo extends Animal implements Herbivore {
    private static String helloText = "splash";
    private String eatText = "munch munch lovely";

    public Hippo(String name) {
        super(name, helloText);
    }

    @Override
    public void eatLeaves() {
        System.out.println(eatText);
    }
}
