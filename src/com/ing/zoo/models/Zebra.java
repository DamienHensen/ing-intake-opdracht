package com.ing.zoo.models;

import com.ing.zoo.interfaces.Herbivore;

public class Zebra extends Animal implements Herbivore {
    private static String helloText = "zebra zebra";
    private String eatText = "munch munch zank yee bra";

    public Zebra(String name) {
        super(name, helloText);
    }

    @Override
    public void eatLeaves() {
        System.out.println(eatText);
    }
}
