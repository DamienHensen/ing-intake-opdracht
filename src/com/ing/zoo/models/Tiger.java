package com.ing.zoo.models;

import java.util.Random;

import com.ing.zoo.interfaces.CanDoTricks;
import com.ing.zoo.interfaces.Carnivore;

public class Tiger extends Animal implements Carnivore, CanDoTricks {
    private static String helloText = "rraaarww";
    private String eatText = "nomnomnom oink wubalubadubdub";
    private String trick;

    public Tiger(String name) {
        super(name, helloText);
    }

    @Override
    public void eatMeat() {
        System.out.println(eatText);
    }

    @Override
    public void performTrick() {
        Random random = new Random();
        int rnd = random.nextInt(2);
        if (rnd == 0) {
            trick = "jumps in tree";
        } else {
            trick = "scratches ears";
        }
        System.out.println(trick);
    }
}
