package com.ing.zoo;

import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Consumer;

import com.ing.zoo.interfaces.CanDoTricks;
import com.ing.zoo.interfaces.Carnivore;
import com.ing.zoo.interfaces.Herbivore;
import com.ing.zoo.models.Animal;
import com.ing.zoo.models.Elephant;
import com.ing.zoo.models.Hippo;
import com.ing.zoo.models.Lion;
import com.ing.zoo.models.Monkey;
import com.ing.zoo.models.Pig;
import com.ing.zoo.models.Tiger;
import com.ing.zoo.models.Zebra;

public class Zoo {
    public HashMap<String, Animal> animals = new HashMap<>();
    public HashMap<String, Consumer<String[]>> commands = new HashMap<>();

    private static final String HELLO_COMMAND = "hello";
    private static final String LEAVES_COMMAND = "give leaves";
    private static final String MEAT_COMMAND = "give meat";
    private static final String TRICK_COMMAND = "perform trick";
    private static final String EXIT_COMMAND = "exit";

    public Zoo() {
        // Set commands
        commands.put(HELLO_COMMAND, this::handleHello);
        commands.put(LEAVES_COMMAND, this::handleLeaves);
        commands.put(MEAT_COMMAND, this::handleMeat);
        commands.put(TRICK_COMMAND, this::handleTrick);

        // Animals
        Lion henk = new Lion("henk");
        Hippo elsa = new Hippo("elsa");
        Pig dora = new Pig("dora");
        Tiger wally = new Tiger("wally");
        Zebra marty = new Zebra("marty");
        Monkey jim = new Monkey("jim");
        Elephant dumbo = new Elephant("dumbo");

        // Add animals to list
        animals.put(henk.toString(), henk);
        animals.put(elsa.toString(), elsa);
        animals.put(dora.toString(), dora);
        animals.put(wally.toString(), wally);
        animals.put(marty.toString(), marty);
        animals.put(jim.toString(), jim);
        animals.put(dumbo.toString(), dumbo);
    }

    public void handleHello(String[] input) {
        if (input.length > 1) {
            handleSpecificHello(input[1].trim());
        } else {
            animals.values().forEach(Animal::sayHello);
        }
    }

    public void handleSpecificHello(String name) {
        try {
            animals.get(name).sayHello();
        } catch (NullPointerException ignored) {
            System.out.println("Unknown animal: " + name);
        }
    }

    public void handleLeaves(String[] input) {
        animals.values().stream()
                .filter(Herbivore.class::isInstance)
                .map(Herbivore.class::cast)
                .forEach(herbivore -> herbivore.eatLeaves());
    }

    public void handleMeat(String[] input) {
        animals.values().stream()
                .filter(Carnivore.class::isInstance)
                .map(Carnivore.class::cast)
                .forEach(herbivore -> herbivore.eatMeat());
    }

    public void handleTrick(String[] input) {
        animals.values().stream()
                .filter(CanDoTricks.class::isInstance)
                .map(CanDoTricks.class::cast)
                .forEach(herbivore -> herbivore.performTrick());
    }

    public static void main(String[] args) {
        Zoo zoo = new Zoo();
        Scanner scanner = new Scanner(System.in);
        boolean running = true;

        while (running) {
            boolean commandFound = false;
            System.out.print("Enter your command: ");
            String input = scanner.nextLine().trim().toLowerCase();

            if (input.equals(EXIT_COMMAND)) {
                System.out.println("Bye");
                running = false;
                break;
            }

            for (String command : zoo.commands.keySet()) {
                if (input.startsWith(command)) {
                    commandFound = true;

                    String[] inputParts = input.split(command);
                    zoo.commands.get(command).accept(inputParts);

                    break;
                }
            }

            if (!commandFound) {
                System.out.println("Unknown command: " + input);
            }

            System.out.println();
        }

        scanner.close();
    }
}
