package com.ing.zoo.interfaces;

public interface Omnivore extends Carnivore, Herbivore {

}