package com.ing.zoo.interfaces;

public interface Herbivore extends Animal {
    public void eatLeaves();
}