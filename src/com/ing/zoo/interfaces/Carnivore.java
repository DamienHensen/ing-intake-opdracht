package com.ing.zoo.interfaces;

public interface Carnivore extends Animal {
    public void eatMeat();
}