package com.ing.zoo.interfaces;

public interface CanDoTricks {
    public void performTrick();
}
